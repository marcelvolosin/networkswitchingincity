from src.placeable.movable.walkable.Walkable import Walkable

class Person(Walkable):

    def __init__(self):
        super(Person, self).__init__()
        self.state = "IDLE"


    def getGeoJson(self):
        '''
        returns structure of data needed when creating geoJSON representation of this object
        @return: structure with object details, use json.dumps(obj.getGeoJson()) to obtain JSON
        '''
        data = {}
        data["id"] = self.id
        data["type"] = "Feature"

        properties = {}
        properties["type"] = "Person"
        properties["state"] = self.state
        properties["id"] = self.id
        properties["gridCoordinates"] = self.location.gridCoordinates
        data["properties"] = properties


        geometry = {}
        geometry["type"] = "Point"
        geometry["coordinates"] = [self.location.longitude, self.location.latitude, self.location.height]
        data["geometry"] = geometry

        json_data = data
        return json_data
