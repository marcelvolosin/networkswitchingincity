from src.city.City import City
from src.common.Location import Location
from src.common.UniqueID import UniqueID


class Placeable:

    def __init__(self):
        uid = UniqueID()
        self.id = uid.getId()
        self.location = Location()
        self.city = "Empty"

    def getLocation(self):
        return self.location;

    def setLocation(self, loc):
        self.location = loc;

    def setCity(self, cit=City):
        self.city = cit
