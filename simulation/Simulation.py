from src.common.Location import Location
from src.common.CommonFunctions import CommonFunctions
from src.MovementModel import MovementModel
import asyncio
import time

guiEnabled = True
print("Movement simulation started!")
fun = CommonFunctions()

location = Location()
location.setLatitude(40.783349)
location.setLongitude(-73.951782)
location.setHeight(0)
nets = [410]  # AT&T Macrocells Only

model = MovementModel(guiEnabled, 200, False, location, nets)
model.addPersonsToModel(5, True)
model.setCudaEnabled(False)

model.sendUpdateToFrontend(True)

async def simulate():
    print("=================== Simulation started ===================")
    start = time.time()
    for i in range(0, 1000):
        # print("step: ",i)
        model.walkStep()
        await asyncio.sleep(1)
        end = time.time()

        elapsed = end - start
    print("================== Simulation finished ===================")
    print("elapsed time:", elapsed)


def main():
    if (guiEnabled):
        loop = asyncio.get_event_loop()
        loop.create_task(simulate())
        loop.run_until_complete(model.frontend.start_server)
        loop.run_forever()
    else:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(simulate())

if __name__ == '__main__':
    main()