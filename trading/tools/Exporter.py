import matplotlib.pylab as plt
import pylab, logging
import pandas as pd

import os
from .pychartdir import *

class Exporter:

    def __init__(self, crnModel, worldModel):
        self.crnModel = crnModel
        self.worldModel = worldModel

        self.MACROCELLS = self.worldModel.macrocells
        self.SMALLCELLS = self.worldModel.femtocells
        self.END_USERS = self.worldModel.endUsers

        self.AVERAGES = pd.DataFrame(
            columns=['MACRO_a_throughput', 'MACRO_sensing_a_rbs', 'MACRO_assigned_a_rbs', 'MACRO_connected_eus',
                     'SMALL_a_throughput', 'SMALL_sensing_a_rbs', 'SMALL_assigned_a_rbs', 'SMALL_connected_eus'])
        self.average = ""

    def export_model_plot(self, macro_c, small_c, end_u):

        self.MACROCELLS = macro_c
        self.SMALLCELLS = small_c
        self.END_USERS = end_u
        x_pos = []
        y_pos = []
        plt.cla()

        # marcel notes
        # changing macrocell.pos[0]
        # pos[0] = longitude
        # pos[1] = latitude


        for macrocell in self.MACROCELLS:
            x_pos.append(macrocell.location.longitude)
            y_pos.append(macrocell.location.latitude)
        for smallcell in self.SMALLCELLS:
            x_pos.append(smallcell.location.longitude)
            y_pos.append(smallcell.location.latitude)
        for enduser in self.END_USERS:
            x_pos.append(enduser.location.longitude)
            y_pos.append(enduser.location.latitude)

        color_array = ['r'] * len(self.MACROCELLS) + ['g'] * len(self.SMALLCELLS) + ['b'] * len(self.END_USERS)
        # markers = ['s'] * len(MACROCELLS) + ['o'] * len(SMALLCELLS) + ['x'] * len(END_USERS)
        # pylab.plot(x_pos, y_pos, c=markers, label='MACROCELLS')
        title = '"red"=MACROCELLS (' + str(len(self.MACROCELLS)) + '), "green"=SMALLCELLS (' + str(
            len(self.SMALLCELLS)) + '), "blue"=EUs (' + str(len(self.END_USERS)) + ')'
        plt.scatter(x_pos, y_pos, marker='o', c=color_array)
        plt.title(str(title))
        # plt.legend()
        F = pylab.gcf()
        # global TIME
        model_title = "/RESULTS/" + self.crnModel.TODAY_STRING + "/model/model_t=" + str(
            self.crnModel.TIME) + ".png"  # ".png"
        #F.savefig(os.getcwd() + model_title, bbox_inches='tight')  # , dpi=300)  # change the dpi
        F.savefig(os.getcwd() + model_title, dpi=300)  # change the dpi
        # plt.cla()
        for cell in (self.MACROCELLS + self.SMALLCELLS):
            for eu in cell.connected_end_users:
                plt.plot([cell.location.longitude, eu.location.longitude], [cell.location.latitude, eu.location.latitude], 'k-')
        F = plt.gcf()
        model_title = "/RESULTS/" +  self.crnModel.TODAY_STRING + "/model/model_t=" + str(self.crnModel.TIME) + "_connections.png"
        F.savefig(os.getcwd() + model_title, dpi=300)  # change the dp
        plt.close()
        pylab.close()

    def export_model_plot_3D(self):
        # MACROCELLS
        x_data_macrocell = []
        y_data_macrocell = []
        z_data_macrocell = []
        for macrocell in self.MACROCELLS:
            x_data_macrocell.append(macrocell.pos[0])
            y_data_macrocell.append(macrocell.pos[1])
            z_data_macrocell.append(macrocell.height_hb)
        # SMALLCELLS
        x_data_smallcell = []
        y_data_smallcell = []
        z_data_smallcell = []
        for smallcell in self.SMALLCELLS:
            x_data_smallcell.append(smallcell.pos[0])
            y_data_smallcell.append(smallcell.pos[1])
            z_data_smallcell.append(smallcell.height_hb)
        # ENDUSERS
        x_data_end_user = []
        y_data_end_user = []
        z_data_end_user = []
        for end_user in self.END_USERS:
            x_data_end_user.append(end_user.pos[0])
            y_data_end_user.append(end_user.pos[1])
            z_data_end_user.append(end_user.height_hb)
        # Create a ThreeDScatterChart object of size 720 x 600 pixels
        c = ThreeDScatterChart(800, 520)
        # Add a title to the chart using 20 points Times New Roman Italic font
        title = '"red"=MACROCELLS (' + str(len(self.MACROCELLS)) + '), "green"=SMALLCELLS (' + str(
            len(self.SMALLCELLS)) + '), "blue"=EUs (' + str(len(self.END_USERS)) + ')'
        c.addTitle(title, "timesi.ttf", 16)
        # Set the center of the plot region at (350, 280), and set width x depth x height to 360 x 360 x 270
        # pixels
        # c.setPlotRegion(350, 280, 360, 360, 270) # original
        c.setPlotRegion(350, 240, 360, 360, 270)
        # Set the elevation and rotation angles to 15 and 30 degrees
        c.setViewAngle(15, 30)
        # Add a legend box at (640, 180)
        c.addLegend(640, 180)
        # Add grey(888888) drop lines to the symbols
        # c.setDropLine(0x888888);
        # Add 3 scatter groups to the chart with 9 pixels glass sphere symbols of red (ff0000), green
        # (00ff00) and blue (0000ff) colors
        c.addScatterGroup(x_data_macrocell, y_data_macrocell, z_data_macrocell, "MACROCELLS", GlassSphere2Shape, 9,
                          0xff0000)
        c.addScatterGroup(x_data_smallcell, y_data_smallcell, z_data_smallcell, "SMALLCELLS", GlassSphere2Shape, 9,
                          0x00ff00)
        c.addScatterGroup(x_data_end_user, y_data_end_user, z_data_end_user, "END_USERS", GlassSphere2Shape, 9,
                          0x0000ff)
        # Add a color axis (the legend) in which the left center is anchored at (645, 270). Set the length
        # to 200 pixels and the labels on the right side.
        # c.setColorAxis(645, 270, Left, 200, Right)
        # Set the x, y and z axis titles using 10 points Arial Bold font
        c.xAxis().setTitle("[m]", "arialbd.ttf", 10)
        c.yAxis().setTitle("[m]", "arialbd.ttf", 10)
        c.zAxis().setTitle("[m]", "arialbd.ttf", 10)
        # Output the chart
        c.makeChart("RESULTS/" + self.crnModel.TODAY_STRING + "/model3D.png")

    def export_Rm_Rs(self):
        Rm = []
        Rs = []
        labels = []
        for end_user in self.END_USERS:
            Rm.append(end_user.Rm)
            Rs.append(end_user.Rs)
            labels.append(str(end_user.unique_id))
        # Create a XYChart object of size 580 x 280 pixels
        c = XYChart(4100, 600)
        # Add a title to the chart using 14 pts Arial Bold Italic font
        c.addTitle("Rm & Rs calculated during sensing for calculating Um & Us", "arialbi.ttf", 14)
        # Set the plot area at (50, 50) and of size 500 x 200. Use two alternative background
        # colors (f8f8f8 and ffffff)
        c.setPlotArea(50, 50, 4000, 500, 0xf8f8f8, 0xffffff)
        # Add a legend box at (50, 25) using horizontal layout. Use 8pts Arial as font, with
        # transparent background.
        c.addLegend(50, 25, 0, "arial.ttf", 8).setBackground(Transparent)
        # Set the x axis labels
        c.xAxis().setLabels(labels)
        # Draw the ticks between label positions (instead of at label positions)
        c.xAxis().setTickOffset(0.5)
        # Add a multi-bar layer with 3 data sets
        layer = c.addBarLayer2(Side)
        layer.addDataSet(Rm, 0xff8080, "Rm")
        layer.addDataSet(Rs, 0x80ff80, "Rs")
        # Set 50% overlap between bars
        layer.setOverlapRatio(0.5)
        # Add a title to the y-axis
        c.yAxis().setTitle("Throughput [Mbps]")
        # Output the chart
        # global TIME
        picture_title = "RESULTS/" + self.crnModel.TODAY_STRING + "/RmRs/RmRs_t=" + str(self.crnModel.TIME) + ".png"
        c.makeChart(picture_title)

    def plot_retail_prices_and_utilities(self):
        utilities = pd.read_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/utilities.csv', index_col=0)
        prices = pd.read_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/retail_prices.csv', index_col=0)

        plt.cla()
        plt.xlabel('time')
        plt.ylabel('retail prices')
        title = 'eu=' + str(len(self.END_USERS)) + " sc=" + str(len(self.SMALLCELLS)) + " gm=" + str(
            self.crnModel.settings.GAMMA) + " max_p=" \
                + str(self.crnModel.settings.MAX_P) + " min_th=" + str(self.crnModel.settings.MIN_TH) + " Mbps"
        plt.title(str(title))
        pylab.plot(prices[['MACROCELLS']], '-b', label='MACROCELLS')
        plt.legend()
        F = pylab.gcf()
        F.savefig(
            os.getcwd() + "/RESULTS/" + self.crnModel.TODAY_STRING + "/macrocells_prices eu=" + str(len(self.END_USERS))
            + " sc=" + str(len(self.SMALLCELLS)) + " gm=" + str(self.crnModel.settings.GAMMA) + ".png",
            dpi=300)  # change the dpi
        plt.cla()
        # plt.close('all')
        # pylab.close()
        # pylab.close('all')

        plt.xlabel('time')
        plt.ylabel('retail prices')
        plt.title(str(title))
        pylab.plot(prices[['SMALLCELLS']], '-r', label='SMALLCELLS')
        plt.legend()
        F = pylab.gcf()
        F.savefig(
            os.getcwd() + "/RESULTS/" + self.crnModel.TODAY_STRING + "/smallcells_prices eu=" + str(len(self.END_USERS))
            + " sc=" + str(len(self.SMALLCELLS)) + " gm=" + str(self.crnModel.settings.GAMMA) + ".png",
            dpi=300)  # change the dpi
        plt.cla()
        # plt.close('all')
        # pylab.close()
        # pylab.close('all')

        plt.xlabel('time')
        plt.ylabel('norm profits')
        plt.title(str(title))
        pylab.plot(utilities[['MACROCELLS']], '-b', label='MACROCELLS')
        plt.legend()
        F = pylab.gcf()
        F.savefig(os.getcwd() + "/RESULTS/" + self.crnModel.TODAY_STRING + "/macrocells_utilities eu=" + str(
            len(self.worldModel.endUsers))
                  + " sc=" + str(len(self.SMALLCELLS)) + " gm=" + str(self.crnModel.settings.GAMMA) + ".png", dpi=300)
        plt.cla()
        # plt.close('all')
        # pylab.close()
        # pylab.close('all')

        plt.xlabel('time')
        plt.ylabel('utilities')
        plt.title(str(title))
        pylab.plot(utilities[['SMALLCELLS']], '-r', label='SMALLCELLS')
        plt.legend()
        F = pylab.gcf()
        F.savefig(
            os.getcwd() + "/RESULTS/" + self.crnModel.TODAY_STRING + "/smallcells_utilities eu=" + str(
                len(self.END_USERS))
            + " sc=" + str(len(self.SMALLCELLS)) + " gm=" + str(self.crnModel.settings.GAMMA) + ".png", dpi=300)
        # plt.close('all')
        # pylab.close()
        # pylab.close('all')
        plt.cla()
        plt.close()

    def export_throughputs_and_rbs(self):
        # columns = ['MACRO_a_throughput', 'MACRO_sensing_a_rbs', 'MACRO_assigned_a_rbs', 'MACRO_connected_eus',
        #            'SMALL_a_throughput', 'SMALL_sensing_a_rbs', 'SMALL_assigned_a_rbs', 'SMALL_connected_eus'])

        connected_eus_macro = 0
        connected_eus_small = 0
        throughput_macro = 0.0
        throughput_small = 0.0
        rbs_sensing_macro = 0.0
        rbs_assigned_macro = 0.0
        rbs_sensing_small = 0.0
        rbs_assigned_small = 0.0
        for macrocell in self.MACROCELLS:
            connected_eus_macro += len(macrocell.connected_end_users)
            for end_user in macrocell.connected_end_users:
                throughput_macro += (end_user.macrocells.loc[macrocell, 'throughput_1RB'] * end_user.assigned_rbs)
                rbs_sensing_macro += end_user.sensing_macrocell_rbs
                rbs_assigned_macro += end_user.assigned_rbs

        if connected_eus_macro:
            self.AVERAGES.loc[self.crnModel.TIME, 'MACRO_a_throughput'] = throughput_macro / connected_eus_macro
            self.AVERAGES.loc[self.crnModel.TIME, 'MACRO_sensing_a_rbs'] = rbs_sensing_macro / connected_eus_macro
            self.AVERAGES.loc[self.crnModel.TIME, 'MACRO_assigned_a_rbs'] = rbs_assigned_macro / connected_eus_macro
            self.AVERAGES.loc[self.crnModel.TIME, 'MACRO_connected_eus'] = connected_eus_macro
        else:
            self.AVERAGES.loc[self.crnModel.TIME, 'MACRO_a_throughput'] = 0
            self.AVERAGES.loc[self.crnModel.TIME, 'MACRO_sensing_a_rbs'] = 0
            self.AVERAGES.loc[self.crnModel.TIME, 'MACRO_assigned_a_rbs'] = 0
            self.AVERAGES.loc[self.crnModel.TIME, 'MACRO_connected_eus'] = 0

        for smallcell in self.SMALLCELLS:
            connected_eus_small += len(smallcell.connected_end_users)
            for end_user in smallcell.connected_end_users:
                throughput_small += (end_user.smallcells.loc[smallcell, 'throughput_1RB'] * end_user.assigned_rbs)
                rbs_sensing_small += end_user.sensing_smallcell_rbs
                rbs_assigned_small += end_user.assigned_rbs
        if connected_eus_small:
            self.AVERAGES.loc[self.crnModel.TIME, 'SMALL_a_throughput'] = throughput_small / connected_eus_small
            self.AVERAGES.loc[self.crnModel.TIME, 'SMALL_sensing_a_rbs'] = rbs_sensing_small / connected_eus_small
            self.AVERAGES.loc[self.crnModel.TIME, 'SMALL_assigned_a_rbs'] = rbs_assigned_small / connected_eus_small
            self.AVERAGES.loc[self.crnModel.TIME, 'SMALL_connected_eus'] = connected_eus_small
        else:
            self.AVERAGES.loc[self.crnModel.TIME, 'SMALL_a_throughput'] = 0
            self.AVERAGES.loc[self.crnModel.TIME, 'SMALL_sensing_a_rbs'] = 0
            self.AVERAGES.loc[self.crnModel.TIME, 'SMALL_assigned_a_rbs'] = 0
            self.AVERAGES.loc[self.crnModel.TIME, 'SMALL_connected_eus'] = 0

        self.AVERAGES.to_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/AVERAGES.csv', mode='w', header=True)

    def calculate_averages_of_throughputs_and_rbs(self, last_items):
        self.average = pd.read_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/AVERAGES.csv', index_col=0)
        self.average.loc['AVERAGE', 'MACRO_a_throughput'] = self.average["MACRO_a_throughput"][-last_items:].mean()
        self.average.loc['AVERAGE', 'MACRO_sensing_a_rbs'] = self.average["MACRO_sensing_a_rbs"][-last_items:].mean()
        self.average.loc['AVERAGE', 'MACRO_assigned_a_rbs'] = self.average["MACRO_assigned_a_rbs"][-last_items:].mean()
        self.average.loc['AVERAGE', 'MACRO_connected_eus'] = self.average["MACRO_connected_eus"][-last_items:].mean()
        self.average.loc['AVERAGE', 'SMALL_a_throughput'] = self.average["SMALL_a_throughput"][-last_items:].mean()
        self.average.loc['AVERAGE', 'SMALL_sensing_a_rbs'] = self.average["SMALL_sensing_a_rbs"][-last_items:].mean()
        self.average.loc['AVERAGE', 'SMALL_assigned_a_rbs'] = self.average["SMALL_assigned_a_rbs"][-last_items:].mean()
        self.average.loc['AVERAGE', 'SMALL_connected_eus'] = self.average["SMALL_connected_eus"][-last_items:].mean()
        self.average.to_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/AVERAGES_FINAL.csv', mode='w', header=True)

    def calculate_averages_of_utilities(self, last_items):
        utilities = pd.read_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/utilities.csv', index_col=0)
        utilities.loc['AVERAGE', 'MACROCELLS'] = utilities["MACROCELLS"][-last_items:].mean()
        utilities.loc['AVERAGE', 'SMALLCELLS'] = utilities["SMALLCELLS"][-last_items:].mean()
        utilities.to_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/utilities_AVERAGE.csv', mode='w', header=True)

    def generate_overall_results(self, time):
        path = "D:/Users/mVolosin/Devel2seria/Switching/Vysledky/RESULTS.csv"
        try:
            RESULTS = pd.read_csv(path, index_col=0)
        except FileNotFoundError:
            RESULTS = pd.DataFrame(
                columns=['tick', 'no_UEs', 'M_price_1RB', 'M_a_utilities', 'M_a_throughput', 'M_sensing_a_rbs',
                         'M_assigned_a_rbs', 'M_connected_eus', 'S_price_1RB', 'S_a_utilities', 'S_a_throughput',
                         'S_sensing_a_rbs', 'S_assigned_a_rbs', 'S_connected_eus'])

        no_eus = len(self.END_USERS)
        averages = pd.read_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/AVERAGES_FINAL.csv', index_col=0)
        prices = pd.read_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/retail_prices.csv', index_col=0)
        utilities = pd.read_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/utilities.csv', index_col=0)
        RESULTS.loc[no_eus, 'no_UEs'] = no_eus
        RESULTS.loc[no_eus, 'tick'] = time

        RESULTS.loc[no_eus, 'M_price_1RB'] = round(prices["MACROCELLS"][-50:].mean(), 3)
        RESULTS.loc[no_eus, 'M_a_utilities'] = round(utilities["MACROCELLS"][-50:].mean(), 3)
        RESULTS.loc[no_eus, 'M_a_throughput'] = round((averages.loc["AVERAGE", "MACRO_a_throughput"] / 1_000_000), 3)
        RESULTS.loc[no_eus, 'M_sensing_a_rbs'] = round(averages.loc["AVERAGE", "MACRO_sensing_a_rbs"], 3)
        RESULTS.loc[no_eus, 'M_assigned_a_rbs'] = round(averages.loc["AVERAGE", "MACRO_assigned_a_rbs"], 3)
        RESULTS.loc[no_eus, 'M_connected_eus'] = round(averages.loc["AVERAGE", "MACRO_connected_eus"], 3)

        RESULTS.loc[no_eus, 'S_price_1RB'] = round(prices["SMALLCELLS"][-50:].mean(), 3)
        RESULTS.loc[no_eus, 'S_a_utilities'] = round(utilities["SMALLCELLS"][-50:].mean(), 3)
        RESULTS.loc[no_eus, 'S_a_throughput'] = round((averages.loc["AVERAGE", "SMALL_a_throughput"] / 1_000_000),
                                                      3)
        RESULTS.loc[no_eus, 'S_sensing_a_rbs'] = round(averages.loc["AVERAGE", "SMALL_sensing_a_rbs"], 3)
        RESULTS.loc[no_eus, 'S_assigned_a_rbs'] = round(averages.loc["AVERAGE", "SMALL_assigned_a_rbs"], 3)
        RESULTS.loc[no_eus, 'S_connected_eus'] = round(averages.loc["AVERAGE", "SMALL_connected_eus"], 3)

        RESULTS = RESULTS.sort_values(['no_UEs'], ascending=[True])
        RESULTS.to_csv(path, mode='w', header=True, )
