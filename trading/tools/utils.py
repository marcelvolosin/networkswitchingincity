from scipy.spatial import distance
import logging


def euclidean(a, b):
    dst = distance.euclidean(a, b)
    return dst


def get_agents(self, agent_type):
    agents = []
    for item in self.schedule.agents:
        if agent_type in str(item):
            agents.append(item)
    return agents


def is_distance_to_smallcells_and_macrocells(model, smallcell_position, SMALL_DISTANCE, MACRO_DISTANCE):
    to_return = True
    SMALLCELLS = get_agents(model, 'agents.Smallcell')
    MACROCELLS = get_agents(model, 'agents.Macrocell')
    if MACROCELLS:
        for mcrclls in MACROCELLS:
            if euclidean(smallcell_position, mcrclls.pos) < MACRO_DISTANCE:
                to_return = False
                break
    if SMALLCELLS:
        for smllcll in SMALLCELLS:
            if euclidean(smallcell_position, smllcll.pos) < SMALL_DISTANCE:
                to_return = False
                break
    return to_return

# def connect_ues_to_cells_based_on_utility():
#     for end_user in model.END_USERS:
#         for row in end_user.macrocells.itertuples(index=True, name='Pandas'):
#             macrocell = row[0]
#             if macrocell.no_of_requests_to_connect < 100:
#                 macrocell.no_of_requests_to_connect = macrocell.no_of_requests_to_connect + 1
#                 end_user.wants_to_connect.append(macrocell)
#                 break
#         for row in end_user.smallcells.itertuples(index=True, name='Pandas'):
#             smallcell = row[0]
#             if smallcell.no_of_requests_to_connect < 20:
#                 smallcell.no_of_requests_to_connect = smallcell.no_of_requests_to_connect + 1
#                 end_user.wants_to_connect.append(smallcell)
#                 break
#     for end_user in model.END_USERS:
#         df = end_user.throughput_data.sort_values('SINR', ascending=False)
#         # logging.info("DF=%s" % df)
#         for i, row in enumerate(df.values):
#             cell = df.index[i]
#             if cell.no_of_available_RBs is not 0:
#                 cell.connected_UEs.append(end_user)
#                 cell.no_of_available_RBs -= 1
#                 end_user.no_assigned_RBs += 1
#                 end_user.connected_to = cell
#                 # logging.info("UE=%s connected_to=%s" % (end_user, end_user.connected_to))
#                 break
#
#
#
#
# def calculate_throughput(model):
#     for end_user in model.end_users:
#         df = end_user.throughput_data.sort_values('SINR', ascending=False)
#         end_user.throughput_to_connected = 180000 * math.log2(1 + df.loc[end_user.connected_to]['SINR'] ) * end_user.no_assigned_RBs
#         logging.info("end_user.throughput_to_connected: %s" % end_user.throughput_to_connected)
#         # logging.info("df.index[end_user.connected_to]=%s no_RB=%s" % (df.loc[end_user.connected_to], end_user.no_assigned_RBs))
#

# logging.info("pocet_small=%s" % pocet_small)
# logging.info("pocet_macro_1=%s" % pocet_macro)
