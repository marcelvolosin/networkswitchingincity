from trading.logic.CrnModel import CrnModel
from trading.Settings import Settings
import asyncio
import time

print(" --------------------- Main script executed")
import platform, sys
print(platform.architecture(), sys.maxsize)
import numpy.distutils.system_info as sysinfo

print("numpy",sysinfo.platform_bits)

guiEnabled = True
crnModel = CrnModel(guiEnabled)





def simulate():
    print("=================== Simulation started ===================")
    settings = Settings()

    start = time.time()
    for i in range(0, settings.MAX_TICKS + 1):
        print("step: ", i)
        crnModel.step()
        end = time.time()

        elapsed = end - start
    print("================== Simulation finished ===================")
    print("elapsed time:", elapsed)
    print("seconds per tick:", elapsed / settings.MAX_TICKS)


simulate()
