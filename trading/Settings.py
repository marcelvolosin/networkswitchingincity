from src.common.Location import Location
import os

class Settings:
    def __init__(self):
        self.strategy = "bargaining"
        self.simulation_time = 30000
        self.debug = False
        self.GAMMA_PRICE_OR_QUALITY = 0.5  # if 1.0 <- quality and if 0.0 <- price
        self.MAXIMUM_PRICE = 101.0
        self.MINIMAL_THROUGHPUT = 0.01  # Mbps

        self.no_smallcells = 40
        self.no_endusers = 50

        try:
            self.no_endusers = int(float(os.getcwd().split('\\')[-2]))
        except:
            self.no_endusers = 111

        #///////////////////////////////////////////////////////
        self.NO_OF_RETAIL_PRICE_STRATEGIES = 50
        self.LEARNING_PARAMETER = 0.03
        self.SMALL_DISTANCE = 100
        self.SMALL_HEIGHT = 2.5
        self.TIME = 1
        #self.MACROCELLS_pos = [(250, 250), (750, 750)]

        self.GAMMA = self.GAMMA_PRICE_OR_QUALITY
        self.MAX_P = self.MAXIMUM_PRICE
        self.MIN_TH = self.MINIMAL_THROUGHPUT
        self.MAX_TICKS = self.simulation_time

        # --- MODEL SETUP ---
        self.MACRO_SMALL_PRICE = 0.5
        self.MACROCELL_RESOURCE_BLOCKS = 80
        self.SMALLCELL_RESOURCE_BLOCKS = 20

        self.location = Location(48.70784, 21.255466)  # location of the center of desired map

        self.location = Location()
        self.location.setLatitude(48.709936)
        self.location.setLongitude(21.238923)
        self.location.setHeight(0)
        self.radius = 500  # radius around the location that will be included
        self.nets = [6]  # load only AT&T base stations
        self.cudaEnabled = False
        self.usersInitialMove = True
        self.guiEnabled = False
