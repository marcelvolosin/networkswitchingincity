from trading.Settings import Settings
from trading.agents.Operator import Operator
from trading.logic.LinearReward import LinearReward
from trading.logic.Sensing import Sensing
from trading.logic.Shopping import Shopping
from trading.logic.WorldModel import WorldModel
from src.common.CommonFunctions import CommonFunctions
import asyncio
import logging
import datetime, os
import json
from trading.tools.Exporter import Exporter


class CrnModel:

    def __init__(self, guiEnabled, seed=10):

        self.TODAY_STRING = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

        os.makedirs(os.getcwd() + "\RESULTS\\" + self.TODAY_STRING)
        os.mkdir(os.getcwd() + "/RESULTS/" + self.TODAY_STRING + "/RmRs")
        os.mkdir(os.getcwd() + "/RESULTS/" + self.TODAY_STRING + "/model")

        self.common = CommonFunctions()
        self.settings = Settings()
        self.operators = []

        self.tick = 0
        self.TIME = 0
        self.MAX_TICKS = self.settings.MAX_TICKS
        self.seed = seed
        print("0")
        if not (self.settings.strategy in ["bargaining", "utility"]):
            raise TypeError("'strategy' must be one of {bargaining, utility}")

        # ---------------- model settings

        self.model = WorldModel(guiEnabled, self.settings.radius, False, self.settings.location,
                                self.settings.nets)  # make instance of model
        self.model.setCudaEnabled(self.settings.cudaEnabled)
        self.model.addEndUsersToModel(self.settings.no_endusers, self.settings.usersInitialMove)
        self.model.guiEnabled = True

        # create femto operators
        femtoOperator = Operator(9999, 'SMALLCELLS')
        # femtoOperator.smallcells = self.model.addSmallCellsToModel(self.settings.no_smallcells,
        #                                                            self.settings.SMALL_DISTANCE,
        #                                                            self.settings.SMALL_HEIGHT, False, femtoOperator)
        femtoOperator.smallcells = self.model.loadSmallCellsFromFile("eugen.json", femtoOperator)
        # femtoOperator.smallcells = self.model.addSmallCellsToModel(self.settings.no_smallcells,
        #                                                            self.settings.SMALL_DISTANCE,
        #                                                            self.settings.SMALL_HEIGHT, False, femtoOperator)
        self.operators.append(femtoOperator)

        # create operator for each NET id
        for net in self.settings.nets:
            self.operators.append(Operator(net, 'MACROCELLS'))
        self.model.pairMacrocellsWithOperators(self.operators)


        self.sensing = Sensing(self)
        self.linearReward = LinearReward(self.model, self)


        self.shopping = Shopping(self)
        self.export = Exporter(self, self.model)
        self.model.printDistancesBetweenPlaceables(self.model.macrocells)

        # locs = ""
        # for item in self.model.femtocells:
        #     locs = locs + item.location.toSingleLine()
        # self.common.appendToFile("femtos2", locs)
        # self.model.storePlaceablesLocationsIntoFile(self.model.femtocells, "bernolakovaFIXED.json")

    def step(self):

        logging.info("------ TICK: %s ------", self.tick)

        # --- SENSING ---
        # self.sensing.init_crn_sensing()
        self.sensing = Sensing(self)
        self.sensing.calculate_and_store_end_users_distances()
        self.sensing.send_requests_to_connect_to_nearest_smallcell_and_macrocell()
        self.sensing.round_robin_of('sensing')
        self.sensing.calculate_Rm_Rs()
        self.linearReward.choose_operators_retail_prices()
        self.sensing.calculate_Um_Us(self.settings.GAMMA)
        # sensing.connected_end_users_based_on_best_utility()

        # --- SHOPPING ---
        self.shopping.smallcell_operator_pay_for_resource_blocks_to_macrocell_operator()
        self.shopping.end_users_shopping()
        self.linearReward.update_retail_learning_probabilities()
        self.sensing.round_robin_of('connection')

        # --- PRINT OPERATOR DETAILS
        # print("-------------------------------")
        # details = []
        # for operator in self.operators:
        #     details.append(operator.getDetails())
        # print(json.dumps(details))

        # --- EXPORTS ---
        self.export.export_throughputs_and_rbs()
        if self.TIME in range(0, self.MAX_TICKS + 1, 500):
            print('-EXPORT-EXECUTED-at-TIME=%s' % self.TIME)
            self.export.export_Rm_Rs()
            self.export.export_model_plot(self.model.macrocells, self.model.femtocells, self.model.endUsers)
            self.export.plot_retail_prices_and_utilities()
            self.export.calculate_averages_of_throughputs_and_rbs(500)
            self.export.calculate_averages_of_utilities(500)  # calc average from the last 400 items
            self.export.generate_overall_results(self.TIME)


        # calculate_throughput(self)
        # bargaining_1day(self)

        # self.schedule.step()
        self.mockedMesaStep()
        # self.datacollector1.collect(self)
        self.tick += 1
        self.TIME += 1

    def mockedMesaStep(self):
        for user in self.model.endUsers:
            user.step()

        for bts in self.model.BTSs:
            bts.step()

        for operator in self.operators:
            operator.step()

        self.model.walkStep()
