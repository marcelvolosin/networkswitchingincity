import random


class Bargaining:

    def __init__(self):
        WHOLESALE_PRICE_1_RB = 0.3
        TRANSACTION_PRICE = 0

    def bargaining_1day(self, model):
        self.init_bargaining(model)
        last_shout_price = "null"
        last_shout_price_type = "null"  # it can be "OFFER" or "BID"
        transaction_price_q = "null"
        seconds = 1
        deal = False
        bid = "null"
        offer = "null"
        buyers = model.small_cells
        sellers = model.macro_cells
        # willing_buyers = []

        while seconds <= 100 and buyers != []:
            for seller in sellers:
                seller.offer = seller.shout_price[-1]
                willing_buyers = []
                deal = False
                for buyer in buyers:
                    buyer.bid = buyer.shout_price[-1]
                    if buyer.bid >= seller.offer:
                        deal = True
                        willing_buyers.append(buyer)

                offer_or_bid = self.choice_offer_or_bid()
                if deal:
                    self.shopping(willing_buyers, buyers, offer_or_bid, seller)
                    self.any_seller_raise_profit_margin(sellers)  # if their shout_price is <= transaction_price_q
                    self.any_buyer_raise_profit_margin(buyers)  # if their shout_price is >= transaction_price_q

                    if "BID" in offer_or_bid:
                        self.sellers_lower_margin(sellers)  # if their shout_price is <= transaction_price_q
                    else:  # "OFFER"
                        self.buyers_lower_margin(buyers)
                else:
                    if buyers is not True:
                        if "BID" in offer_or_bid:
                            sellected_buyer = self.get_random_item_from_list(buyers)
                            TRANSACTION_PRICE = sellected_buyer.shout_price[-1]
                            self.buyers_lower_margin(buyers)
                        else:
                            TRANSACTION_PRICE = seller.shout_price[-1]
                            self.sellers_lower_margin(sellers)  # if their shout_price is <= transaction_price_q

    def init_bargaining(self, model):
        # S - E - L - L - E - R - s
        for macrocell in model.macro_cells:
            macrocell.limit_price = self.WHOLESALE_PRICE_1_RB
            macrocell.learning_rate = random.uniform(0.1, 0.5)  # remains fixed for the duration of the experiment
            macrocell.momentum_coefficient = random.uniform(0.2,
                                                            0.8)  # remains fixed for the duration of the experiment
            # -------------------------------------------------------------------------------------------------------------
            macrocell.profit_margin = random.uniform(0.85, 0.75)
            macrocell.shout_price = []
            macrocell.shout_price.append(
                self.calc_shout_price(macrocell.limit_price, macrocell.profit_margin))  # BID of BUYER]

        # B - U - Y - E - R - s
        for smallcell in model.small_cells:
            smallcell.old_limit_price = smallcell.limit_price
            smallcell.limit_price = 0.5  # new_retail_price
            if smallcell.limit_price == 0:
                smallcell.limit_price = smallcell.old_one_limit_price
            smallcell.learning_rate = random.uniform(0.1, 0.5)  # remains fixed for the duration of the experiment
            smallcell.momentum_coefficient = random.uniform(0.2,
                                                            0.8)  # remains fixed for the duration of the experiment
            # -------------------------------------------------------------------------------------------------------------
            smallcell.profit_margin = random.uniform(-0.85, -0.75)
            smallcell.shout_price = []
            smallcell.shout_price.append(
                self.calc_shout_price(macrocell.limit_price, macrocell.profit_margin))  # BID of BUYER]

    def calc_shout_price(self, limit_price, profit_margin):
        return limit_price * (1 + profit_margin)

    def choice_offer_or_bid(self):
        probability = random.uniform(0, 1)

        if probability >= 0.5:
            return "OFFER"
        else:
            return "BID"

    def any_seller_raise_profit_margin(self, sellers):
        for seller in sellers:
            if seller.shout_price[-1] <= self.TRANSACTION_PRICE:
                self.increase_profit_margin(seller)
                # correct_shout_price_to_M_limit_price

    def any_buyer_raise_profit_margin(self, buyers):
        for buyer in buyers:
            if buyer.shout_price[-1] >= self.TRANSACTION_PRICE:
                self.increase_profit_margin(buyer)
                # correct_shout_price_to_F_limit_price

    def increase_profit_margin(self, seller):
        Ai = random.uniform(0.0, 0.05)
        Ri = random.uniform(1.0, 1.05)
        target_price = (Ri * self.TRANSACTION_PRICE) + Ai
        delta_value = seller.learning_rate * (self.TRANSACTION_PRICE - seller.shout_price[-1])
        momentum = (seller.momentum_coeff * seller.momentum_old + ((1 - seller.momentum_coeff) * delta_value))
        momentum_old = momentum
        seller.profit_margin = (((seller.shout_price[-1] + momentum) / seller.limit_price) - 1)
        seller.shout_price.append(self.calc_shout_price(seller.limit_price, seller.profit_margin))

    def decrease_profit_margin(self, seller):
        Ai = random.uniform(-0.05, 0.0)
        Ri = random.uniform(0.95, 1.00)
        target_price = (Ri * self.TRANSACTION_PRICE) + Ai
        delta_value = seller.learning_rate * (self.TRANSACTION_PRICE - seller.shout_price[-1])
        momentum = (seller.momentum_coeff * seller.momentum_old + ((1 - seller.momentum_coeff) * delta_value))
        momentum_old = momentum
        seller.profit_margin = (((seller.shout_price[-1] + momentum) / seller.limit_price) - 1)
        seller.shout_price.append(self.calc_shout_price(seller.limit_price, seller.profit_margin))

    def get_random_item_from_list(self, items_list):
        # logging.info("items_list: %s" % items_list)
        the_item = random.choice(items_list)
        # logging.info("the_item: %s" % str(the_item))
        return the_item

    def buyers_lower_margin(self, buyers):
        for buyer in buyers:
            if buyer.shout_price[-1] <= self.TRANSACTION_PRICE:
                self.decrease_profit_margin(buyer)

    def sellers_lower_margin(self, sellers):
        for seller in sellers:
            if seller.shout_price[-1] <= self.TRANSACTION_PRICE:
                self.decrease_profit_margin(seller)

    def shopping(self, willing_buyers, buyers, offer_or_bid, seller):
        willing_buyer = self.get_random_item_from_list(willing_buyers)
        if "BID" in offer_or_bid:
            TRANSACTION_PRICE = willing_buyer.shout_price[-1]
        else:
            TRANSACTION_PRICE = seller.shout_price[-1]

        payment = 24 * TRANSACTION_PRICE * willing_buyer.smallcell_allocated_rbs
        willing_buyer.profit_1day -= payment
        willing_buyer.budget -= payment

        seller.profit_1day += payment
        seller.budget += payment
        buyers.remove(willing_buyer)
