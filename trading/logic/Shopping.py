import logging

from trading.Settings import Settings


class Shopping:
    def __init__(self, crnModel):
        self.crnModel = crnModel
        self.settings = Settings()

    def smallcell_operator_pay_for_resource_blocks_to_macrocell_operator(self):
        # logging.debug('')
        # logging.debug('------ SMALLCELLS PAY FOR RBs TO MACROCELLS -----')
        # no_of_smallcells = layout_df.loc['no_smallcells', 'SMALLCELLS']
        macro_small_payment = self.settings.SMALLCELL_RESOURCE_BLOCKS * self.settings.MACRO_SMALL_PRICE * self.settings.no_smallcells  # * no_of_smallcells
        # logging.debug("macro_small_payment: %s", macro_small_payment)
        for operator in self.crnModel.operators:
            if 'SMALLCELLS' == operator.owner:
                operator.cost = macro_small_payment

    def end_users_shopping(self):
        # logging.debug('')
        # logging.debug('------ SHOPPING -----')
        for end_user in self.crnModel.model.endUsers:
            cell = end_user.connected_to_cell
            if cell:
                if cell.type == "Femtocell":
                    no_RBs = end_user.sensing_smallcell_rbs
                else:
                    # print("choosing macro")
                    no_RBs = end_user.sensing_macrocell_rbs

                payment = cell.new_retail_price * no_RBs
                # cell.owner.income += payment
                cell.operator.income += payment
                # logging.debug("end_user.connected_to_cell: %s", cell)
                # logging.debug("new_retail_price: %s", cell.new_retail_price)
                # logging.debug("owner: %s", cell.owner)
                # logging.debug("no_RBs: %s", no_RBs)
                # logging.debug("payment: %s", payment)
                # logging.debug("cell.operator.income: %s", cell.operator.income)
                # logging.debug('------')
