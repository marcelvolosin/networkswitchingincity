from trading.logic import CrnModel
from trading.logic.Throughput import Throughput
from trading.logic.WorldModel import WorldModel
from src.common.CommonFunctions import CommonFunctions

import logging
import numpy
import random


class Sensing:

    def __init__(self, crnModel):
        self.worldModel = crnModel.model
        self.crnModel = crnModel
        self.END_USERS = self.worldModel.endUsers
        self.SMALLCELLS = self.worldModel.femtocells
        self.MACROCELLS = self.worldModel.macrocells
        self.OPERATORS = crnModel.operators
        self.com = CommonFunctions()
        self.settings = crnModel.settings
        self.throughput = Throughput()

    def calculate_and_store_end_users_distances(self):
        for end_user in self.END_USERS:
            for smallcell in self.SMALLCELLS:
                distance = self.com.get3dDistance(end_user.location, smallcell.location) / 1000
                # print(distance)
                # distance = (euclidean(smallcell.pos, end_user.pos)) / 1000
                end_user.smallcells.loc[smallcell, 'distance'] = distance
            end_user.smallcells = end_user.smallcells.sort_values('distance')
            # logging.debug('====================')
            # logging.debug(end_user.smallcells)
            for macrocell in self.MACROCELLS:
                distance = self.com.getReal2dDistance(end_user.location, macrocell.location) / 1000
                # distance = (euclidean(macrocell.pos, end_user.pos)) / 1000
                end_user.macrocells.loc[macrocell, 'distance'] = distance
            end_user.macrocells = end_user.macrocells.sort_values('distance')
            # logging.debug(end_user.macrocells)

    def send_requests_to_connect_to_nearest_smallcell_and_macrocell(self):
        # logging.debug('')
        # logging.debug('------ SEND_REQUESTS_TO_CONNECT_TO_M&S (sensing.py: send_requests_to_connect_to_nearest_smallcell_and_macrocell()) ------')
        EUs = self.END_USERS[:]
        while EUs:
            secure_random = random.SystemRandom()
            end_user = secure_random.choice(EUs)
            for row in end_user.macrocells.itertuples(index=True, name='Pandas'):
                macrocell = row[0]
                if len(macrocell.potential_end_users) < self.settings.MACROCELL_RESOURCE_BLOCKS:
                    macrocell.potential_end_users.append(end_user)
                    end_user.potential_macrocell_to_connect = macrocell
                    break
            for row in end_user.smallcells.itertuples(index=True, name='Pandas'):
                smallcell = row[0]
                if len(smallcell.potential_end_users) < self.settings.SMALLCELL_RESOURCE_BLOCKS:
                    smallcell.potential_end_users.append(end_user)
                    end_user.potential_smallcell_to_connect = smallcell
                    break
            EUs.remove(end_user)

        # for macrocell in MACROCELLS:
        #     logging.debug('')
        #     logging.debug('--  %s' % macrocell)
        #     logging.debug('---->  potential_end_users %s' % macrocell.potential_end_users)
        #     logging.debug('---->  no: %s end_users' % len(macrocell.potential_end_users))
        #
        # for smallcell in SMALLCELLS:
        #     logging.debug('')
        #     logging.debug('--  %s' % smallcell)
        #     logging.debug('---->  potential_end_users %s' % smallcell.potential_end_users)
        #     logging.debug('---->  no: %s end_users' % len(smallcell.potential_end_users))

    def round_robin_of(self, part):
        # logging.debug('')
        # logging.debug('---- RR_DURING_SENSING (sensing.py: round_robin_of(%s)) ---- ' % part)
        if 'sensing' in part:
            for cell in self.MACROCELLS:
                no_rbs = 0
                if cell.potential_end_users:
                    while no_rbs < cell.no_of_available_RBs:
                        for potential_eu in cell.potential_end_users:
                            potential_eu.sensing_macrocell_rbs += 1
                            no_rbs += 1
                            if no_rbs is cell.no_of_available_RBs:
                                break
            for cell in self.SMALLCELLS:
                no_rbs = 0
                if cell.potential_end_users:
                    while no_rbs < cell.no_of_available_RBs:
                        for potential_eu in cell.potential_end_users:
                            potential_eu.sensing_smallcell_rbs += 1
                            no_rbs += 1
                            if no_rbs is cell.no_of_available_RBs:
                                break
            # for eu in END_USERS:
            #     logging.debug('eu: %s macro_RBs: %s small_RBs: %s' % (eu, eu.sensing_macrocell_rbs, eu.sensing_smallcell_rbs))
        else:
            for cell in self.MACROCELLS:
                no_rbs = 0
                if cell.connected_end_users:
                    while no_rbs < cell.no_of_available_RBs:
                        for eu in cell.connected_end_users:
                            eu.assigned_rbs += 1
                            no_rbs += 1
                            if no_rbs is cell.no_of_available_RBs:
                                break
            for cell in self.SMALLCELLS:
                no_rbs = 0
                if cell.connected_end_users:
                    while no_rbs < cell.no_of_available_RBs:
                        for eu in cell.connected_end_users:
                            eu.assigned_rbs += 1
                            no_rbs += 1
                            if no_rbs is cell.no_of_available_RBs:
                                break
            # for cell in MACROCELLS:
            #     logging.debug('')
            #     logging.debug('CELL: %s' % cell)
            #     for eu in cell.connected_end_users:
            #         logging.debug('eu: %s connected to: %s assigned RBs: %s' % (eu, eu.connected_to_cell, eu.assigned_rbs))
            # for cell in SMALLCELLS:
            #     logging.debug('')
            #     logging.debug('CELL: %s' % cell)
            #     for eu in cell.connected_end_users:
            #         logging.debug('eu: %s connected to: %s assigned RBs: %s' % (eu, eu.connected_to_cell, eu.assigned_rbs))

    def calculate_Rm_Rs(self):
        # logging.debug('')
        # logging.debug('------- THROUGHPUTS (sensing.py: calculate_Rm_Rs()) -------')
        for end_user in self.END_USERS:
            # logging.debug('------- %s - pos: %s' % (end_user, end_user.pos))

            # --- CALC Rm MACROCELL ---
            macrocell = end_user.potential_macrocell_to_connect
            #print("enduserov macrocell", macrocell)
            if macrocell:
                # logging.debug('%s - pos: %s' % (macrocell, macrocell.pos))
                # if numpy.isnan(end_user.macrocells.loc[macrocell, 'throughput_1RB']):
                self.throughput.calculate_throughput_of_one_resource_block_to_macrocell(end_user, macrocell)
                # logging.debug('Rm 1-RB %s Mbps' % round((end_user.macrocells.loc[macrocell, 'throughput_1RB'] / 1000000), 2))
                end_user.Rm = round(
                    ((end_user.macrocells.loc[macrocell, 'throughput_1RB'] * end_user.sensing_macrocell_rbs) / 1000000),
                    2)
                #print("RM", end_user.Rm)
                # logging.debug('Rm %s-RB %s Mbps' % (end_user.sensing_macrocell_rbs, end_user.Rm))

            # --- CALC Rs SMALLCELLS ---
            smallcell = end_user.potential_smallcell_to_connect
            if smallcell:
                # logging.debug('%s - pos: %s' % (smallcell, smallcell.pos))
                # if numpy.isnan(end_user.smallcells.loc[smallcell, 'throughput_1RB']):
                self.throughput.calculate_throughput_of_one_resource_block_to_smallcell(end_user, smallcell)
                # logging.debug('Rs 1-RB %s Mbps' % round((end_user.smallcells.loc[smallcell, 'throughput_1RB'] / 1000000), 2))
                end_user.Rs = round(
                    ((end_user.smallcells.loc[smallcell, 'throughput_1RB'] * end_user.sensing_smallcell_rbs) / 1000000),
                    2)
                #print("RS",end_user.Rs)
                # logging.debug('Rs %s-RB %s Mbps' % (end_user.sensing_smallcell_rbs, end_user.Rs))

    def calculate_Um_Us(self, gama_parameter):
        # logging.debug('')
        # logging.debug('------- UTILITIES (sensing.py: calculate_Um_Us(gama_parameter)) -------')
        for end_user in self.END_USERS:
            # logging.debug('------- %s' % end_user)

            # --- CALC Um MACROCELL ---
            macrocell = end_user.potential_macrocell_to_connect
            # logging.debug(macrocell)
            if macrocell:
                if numpy.isnan(end_user.macrocells.loc[macrocell, 'throughput_1RB']):
                    self.throughput.calculate_throughput_of_one_resource_block_to_macrocell(end_user, macrocell)

                step_func_price = self.step_function(self.settings.MAXIMUM_PRICE,
                                                     macrocell.new_retail_price * end_user.sensing_macrocell_rbs)
                step_func_through = self.step_function(end_user.Rm, self.settings.MINIMAL_THROUGHPUT)
                if end_user.Rm > 5.0:
                    end_user.Rm = 5.0
                end_user.Um = step_func_price * step_func_through * (
                        gama_parameter * (end_user.Rm - self.settings.MINIMAL_THROUGHPUT) + (1 - gama_parameter) * (
                        self.settings.MAXIMUM_PRICE - macrocell.new_retail_price * end_user.sensing_macrocell_rbs))
                if end_user.Um < 0.0:
                    end_user.Um = 0.0
                # logging.debug('Um %s bezrozmerne' % end_user.Um)

            # --- CALC Us SMALLCELLS ---
            smallcell = end_user.potential_smallcell_to_connect
            # logging.debug(smallcell)
            if smallcell:
                if numpy.isnan(end_user.smallcells.loc[smallcell, 'throughput_1RB']):
                    self.throughput.calculate_throughput_of_one_resource_block_to_smallcell(end_user, smallcell)

                step_func_price = self.step_function(self.settings.MAXIMUM_PRICE,
                                                     smallcell.new_retail_price * end_user.sensing_smallcell_rbs)
                step_func_through = self.step_function(end_user.Rs, self.settings.MINIMAL_THROUGHPUT)
                if end_user.Rs > 5.0:
                    end_user.Rs = 5.0
                end_user.Us = step_func_price * step_func_through * (
                        gama_parameter * (end_user.Rs - self.settings.MINIMAL_THROUGHPUT) + (1 - gama_parameter) * (
                        self.settings.MAXIMUM_PRICE - smallcell.new_retail_price * end_user.sensing_smallcell_rbs))
                if end_user.Us < 0.0:
                    end_user.Us = 0.0
                # logging.debug('Us %s bezrozmerne' % end_user.Us)

            # print ("-------")
            # print("Um:", end_user.Um)
            # print("Us:", end_user.Us)
            # if end_user.Um and end_user.Us:
            if end_user.Um > end_user.Us:
                end_user.connected_to_cell = end_user.potential_macrocell_to_connect
                end_user.potential_macrocell_to_connect.connected_end_users.append(end_user)
            if end_user.Um < end_user.Us:
                end_user.connected_to_cell = end_user.potential_smallcell_to_connect
                end_user.potential_smallcell_to_connect.connected_end_users.append(end_user)
            # if end_user.connected_to_cell:
            #     logging.debug('-- conected to --> %s with no of end_users %s' % (end_user.connected_to_cell.owner, len(end_user.connected_to_cell.connected_end_users)))
            # else:
            #     logging.debug('-- conected to --> NONE')
            # logging.debug('')

    def step_function(self, value_1, value_2):
        if value_1 - value_2 > 0:
            return 1
        else:
            return 0

    def connected_end_users_based_on_best_utility(self):
        logging.debug('')
        # logging.debug('------ CONNECTED_end_users_based_on_best_utility (sensing.py: connected_end_users_based_on_best_utility()) -----')
        #
        # for macrocell in MACROCELLS:
        #     logging.debug('')
        #     logging.debug('--  %s' % macrocell)
        #     for eu in macrocell.connected_end_users:
        #         logging.debug('---->  connected_end_users %s' % eu)
        #     logging.debug('connected eus=%s' % len(macrocell.connected_end_users))
        #
        # for smallcell in SMALLCELLS:
        #     logging.debug('')
        #     logging.debug('--  %s' % smallcell)
        #     for eu in smallcell.connected_end_users:
        #         logging.debug('---->  connected_end_users %s' % eu)
        #     logging.debug('connected eus=%s' % len(smallcell.connected_end_users))
