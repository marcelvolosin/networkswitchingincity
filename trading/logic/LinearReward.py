import pandas as pd
import random

from trading.Settings import Settings
from trading.logic import CrnModel
from trading.logic.WorldModel import WorldModel


class LinearReward:
    def __init__(self, worldModel=WorldModel, crnModel=CrnModel):
        self.settings = Settings()
        self.worldmodel = worldModel
        self.crnModel = crnModel

        self.UTILITIES = pd.DataFrame(columns=['MACROCELLS', 'SMALLCELLS'])
        self.PRICES = pd.DataFrame(columns=['MACROCELLS', 'SMALLCELLS'])
        for operator in self.crnModel.operators:
            operator.retail_prices_probabilities = self.init_retail_prices_probabilities(
                self.settings.NO_OF_RETAIL_PRICE_STRATEGIES)


    def init_retail_prices_probabilities(self, no_of_price_strategies):
        retail_price = 5.0 / no_of_price_strategies
        probability = 1.0 / no_of_price_strategies
        retail_prices_probabilities = pd.DataFrame(
            columns=['strategy', 'retail_price', 'new_probability', 'old_probability'])
        for i in range(1, no_of_price_strategies + 1):
            retail_prices_probabilities.loc[i, 'strategy'] = i
            retail_prices_probabilities.loc[i, 'retail_price'] = retail_price * i
            retail_prices_probabilities.loc[i, 'new_probability'] = probability
            retail_prices_probabilities.loc[i, 'old_probability'] = probability
            # retail_prices_probabilities.round({'new_probability': 2, 'old_probability': 2})
        # logging.info(retail_prices_probabilities)
        return retail_prices_probabilities

    def choose_operators_retail_prices(self):
        # logging.debug('')
        # logging.debug('------ CHOOSE RETAIL PRICES (linear_reward.py: choose_operators_retail_prices()) ------')
        for operator in self.crnModel.operators:
            # logging.debug(operator.owner)
            operator.chosen_retail_price_strategy = None
            current_probability_start = 0.0
            current_probability_end = 0.0
            # random_probability = random.uniform(0,1)
            # logging.info('random1 %s' % random_probability)
            secure_random = random.SystemRandom()
            random_probability = secure_random.uniform(0.0000, 1.0000)
            # logging.info('random2 %s' % random_probability2)
            # logging.info('random_probability %s' % random_probability)
            i = 1

            for i in range(1, self.settings.NO_OF_RETAIL_PRICE_STRATEGIES + 1):
                current_probability_end += operator.retail_prices_probabilities.loc[i, 'new_probability']
                # logging.info('--------------start: %s' % current_probability_start)
                # logging.info('--------------i: %s' % i)
                # logging.info('--------------end: %s' % current_probability_end)
                if current_probability_start <= random_probability <= current_probability_end:
                    operator.new_retail_price = operator.retail_prices_probabilities.loc[i, 'retail_price']
                    operator.chosen_retail_price_strategy = i
                    # logging.debug("operator.chosen_retail_price_strategy %s" % operator.chosen_retail_price_strategy)
                    # logging.debug("operator.new_retail_price %s" % operator.new_retail_price)
                    # logging.info('%s' % random_probability)
                    break
                current_probability_start += operator.retail_prices_probabilities.loc[i, 'new_probability']

            for cell in (self.worldmodel.BTSs):
                # print("cell.owner", cell.owner)
                # print("operator owner", operator.owner)
                if cell.owner == operator.owner:
                    # print("calculating new retail")
                    cell.new_retail_price = operator.new_retail_price
                    # logging.info('cell: %s cell.new_retail_price: %s' % (cell.owner, cell.new_retail_price))

    def update_retail_learning_probabilities(self):
        global NORM_PROFIT
        # logging.debug('')
        # logging.debug('------ UPDATE_RETAIL_LEARNING_PROBABILITIES (linear_reward.py: update_retail_learning_probabilities()) ------')
        for operator in self.crnModel.operators:
            # logging.debug('')
            # logging.debug('------ %s:' % operator.owner)
            # BACKUP the old probabilities
            operator.retail_prices_probabilities['old_probability'] = operator.retail_prices_probabilities[
                'new_probability']
            # CALC the new probabilities
            no_RBs = 0
            for cell in (operator.macrocells + operator.smallcells):
                no_RBs += cell.no_of_available_RBs
            # logging.info('noRBs %s' % no_RBs)
            max_profit = no_RBs * 5.0 # highest price for 1 RB is 5.0
            utility_1tick = operator.income - operator.cost
            # logging.debug('utility_1tick %s' % utility_1tick)
            # if utility_1tick < 0:
            #     utility_1tick = 0

            norm_profit = utility_1tick / (max_profit - operator.cost)
            if norm_profit < 0:
                norm_profit = 0
            if 'MACROCELLS' == operator.owner:
                self.UTILITIES.loc[self.crnModel.TIME, 'MACROCELLS'] = utility_1tick + (
                        self.settings.SMALLCELL_RESOURCE_BLOCKS * self.settings.MACRO_SMALL_PRICE * self.settings.no_smallcells)
                self.PRICES.loc[self.crnModel.TIME, 'MACROCELLS'] = operator.new_retail_price

            if 'SMALLCELLS' == operator.owner:
                self.UTILITIES.loc[self.crnModel.TIME, 'SMALLCELLS'] = utility_1tick
                self.PRICES.loc[self.crnModel.TIME, 'SMALLCELLS'] = operator.new_retail_price
            # with open('plots/foo.csv', 'a') as f:
            #
            #     NORM_PROFIT.to_csv(f, header=False)

            # with open('plots/foo.csv', 'a') as f:
            #     (df + 6).to_csv(f, header=False)
            # logging.debug('norm_profit %s' % norm_profit)
            # if norm_profit > 1:
            #     logging.info("ERROR during NORMALIZATION -> norm_profit: %s" % norm_profit)

            for i in range(1, self.settings.NO_OF_RETAIL_PRICE_STRATEGIES + 1):
                old_probability = operator.retail_prices_probabilities.loc[i, 'old_probability']
                # if operator.chosen_retail_price_strategy is 10 or operator.chosen_retail_price_strategy is 8:
                # logging.info("old_probability: %s" % old_probability)
                # logging.info("operator.chosen_retail_price_strategy: %s" % operator.chosen_retail_price_strategy)
                # logging.info("-------------------------> utility_1tick: %s" % utility_1tick)
                # logging.info("-------------------------> norm_profit: %s" % norm_profit)

                # ------ CASE -> NOT_EQUALS
                if i != operator.chosen_retail_price_strategy:
                    # logging.info('NOT %s' % old_probability)
                    new_probability = old_probability - (
                                self.settings.LEARNING_PARAMETER * norm_profit * old_probability)
                    operator.retail_prices_probabilities.loc[i, 'new_probability'] = new_probability
                    # if new_probability > 1:
                    #     logging.info('NOT_EQUALS bigger new_probability: %s' % new_probability)
                    #     logging.info('operator: %s' % operator.owner)

                # ------ CASE -> EQUALS
                else:
                    sum_of_probabilities = 0
                    # logging.info('operator.chosen_retail_price_strategy: %s' % operator.chosen_retail_price_strategy)
                    # logging.info('sum_of_probabilities: %s' % (sum_of_probabilities))
                    for j in range(1, self.settings.NO_OF_RETAIL_PRICE_STRATEGIES + 1):
                        if j != operator.chosen_retail_price_strategy:
                            # logging.info('------> old_probab: %s' % operator.retail_prices_probabilities.loc[j, 'old_probability'])
                            sum_of_probabilities = sum_of_probabilities + operator.retail_prices_probabilities.loc[
                                j, 'old_probability']
                            # logging.info('j: %s - sum_of_probabilities: %s' % (j, sum_of_probabilities))
                    # if abs(sum_of_probabilities) > 1:
                    #     logging.info("sum_of_probabilities %s" % sum_of_probabilities)
                    # logging.info('IS %s' % old_probability)
                    new_probability = old_probability + (
                                self.settings.LEARNING_PARAMETER * norm_profit * sum_of_probabilities)
                    operator.retail_prices_probabilities.loc[i, 'new_probability'] = new_probability
                    # if new_probability > 1:
                    # logging.info('EQUALS bigger new_probability: %s' % new_probability)
                    # logging.info('operator: %s' % operator.owner)
                    # logging.info("sum_of_probabilities %s" % sum_of_probabilities)
                    # logging.info("norm_profit %s" % norm_profit)
                    # logging.info("old_probability %s" % old_probability)
            # Total = operator.retail_prices_probabilities['old_probability'].sum()
            # logging.info('Total: %s' % (Total))
            # logging.info(NORM_PROFIT)
            self.UTILITIES.to_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/utilities.csv', mode='w', header=True)
            self.PRICES.to_csv('RESULTS/' + self.crnModel.TODAY_STRING + '/retail_prices.csv', mode='w', header=True)

        # for operator in model.OPERATORS:
        # logging.info('operator: %s' % operator.owner)
        # logging.info(operator.retail_prices_probabilities)
        # total = operator.retail_prices_probabilities['new_probability'].sum()
        # logging.info('total: %s' % total)
        # if total < 0.99:
        #     for i in range(1, model.NO_OF_RETAIL_PRICE_STRATEGIES+1):
        #         operator.retail_prices_probabilities.loc[i, 'new_probability'] /= total
        #     Total = operator.retail_prices_probabilities['new_probability'].sum()
        #     logging.info('new Total: %s' % Total)
        # if total > 1.01:
        #     for i in range(1, model.NO_OF_RETAIL_PRICE_STRATEGIES+1):
        #         operator.retail_prices_probabilities.loc[i, 'new_probability'] /= total
        #     Total = operator.retail_prices_probabilities['new_probability'].sum()
        #     logging.info('new Total: %s' % Total)
        # logging.info('retail_prices_probabilities: %s' % operator.retail_prices_probabilities)
