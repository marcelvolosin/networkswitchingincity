import math


class Throughput:
    def __init__(self):
        self.MOBILE_STATION_HEIGHT = 1.7
        # BANDWIDTH = 0.18 * math.pow(10, 6)
        # BOLTZMAN = 1.38 * math.pow(10, -23)
        # THERMODYN = 290
        self.NOISE_TARAS = 1.8 * math.pow(10, -12)

    def calculate_throughput_of_one_resource_block_to_macrocell(self, end_user, actual_macrocell):
        distance_to_actual = end_user.macrocells.loc[actual_macrocell, 'distance']
        # logging.debug('distance_to_actual macrocell %s Km' % distance_to_actual)

        log10_f = math.log10(actual_macrocell.carrier_frequency_f)
        log10_hb = math.log10(actual_macrocell.height_hb)
        Tx_dBm = 10 * math.log10(actual_macrocell.transmit_power_Pi) + 30
        correction = 0.8 + (1.1 * log10_f - 0.7) * self.MOBILE_STATION_HEIGHT - 1.56 * log10_f

        # calculate total interference of causing by other smallcells
        total_interference_mW = 0.0
        for macrocell in end_user.macrocells.itertuples(index=True, name='Pandas'):
            if macrocell[0] != actual_macrocell:
                distance = getattr(macrocell, "distance")
                if distance < 0.0009:
                    distance = 0.001
                # logging.debug('end user distance to macrocell causing interference: %s' % distance)
                path_loss_interference = 69.55 + 26.16 * log10_f - 13.82 * log10_hb - correction + (
                        44.9 - 6.55 * log10_hb) * math.log10(distance)
                # logging.debug('path_loss_interference: %s ' % path_loss_interference)
                interference_dB = Tx_dBm - path_loss_interference
                # logging.debug('interference_dB: %s ' % interference_dB)
                interference_mW = math.pow(10, (interference_dB / 10))
                # logging.debug('interference_mW: %s ' % interference_mW)
                total_interference_mW = total_interference_mW + interference_mW
        # logging.debug('total_interference_mW: %s' % total_interference_mW)

        # calculate SINR
        if distance_to_actual < 0.0009:
            distance_to_actual = 0.001
        path_loss_hata = 69.55 + 26.16 * log10_f - 13.82 * log10_hb - correction + (
                44.9 - 6.55 * log10_hb) * math.log10(
            distance_to_actual)
        # logging.debug('path_loss_hata: %s ' % path_loss_hata)
        P_received_dB = Tx_dBm - path_loss_hata
        # logging.debug('P_received_dB: %s ' % P_received_dB)
        P_received_mW = math.pow(10, (P_received_dB / 10))
        # logging.debug('P_received_mW: %s ' % P_received_mW)
        # P_noise_mW = BOLTZMAN * BANDWIDTH * THERMODYN * 0.001
        # logging.debug('P_noise_mW: %s ' % P_noise_mW)
        sinr = P_received_mW / (total_interference_mW + self.NOISE_TARAS)  # P_noise_mW)
        # logging.debug('sinr_dB: %s ' % sinr_dB)

        # throughput:
        throughput = 180000 * math.log2(1 + sinr) * 1  # 1 resource_block will got everyone
        end_user.macrocells.loc[actual_macrocell, 'throughput_1RB'] = throughput
        # logging.debug("throughput to smallcell: %s " % throughput)
        return throughput

    def calculate_throughput_of_one_resource_block_to_smallcell(self, end_user, actual_smallcell):
        distance_to_actual = end_user.smallcells.loc[actual_smallcell, 'distance']
        # logging.debug('distance_to_actual smallcell %s Km' % distance_to_actual)

        log10_f = math.log10(actual_smallcell.carrier_frequency_f)
        log10_hb = math.log10(actual_smallcell.height_hb)
        Tx_dBm = 10 * math.log10(actual_smallcell.transmit_power_Pi) + 30
        correction = 0.8 + (1.1 * log10_f - 0.7) * self.MOBILE_STATION_HEIGHT - 1.56 * log10_f

        # calculate total interference of causing by other smallcells
        total_interference_mW = 0.0
        for smallcell in end_user.smallcells.itertuples(index=True, name='Pandas'):
            if smallcell[0] != actual_smallcell:
                distance = getattr(smallcell, "distance")
                if distance < 0.0009:
                    distance = 0.001
                # logging.debug('end user distance to smallcell causing interference: %s' % distance)
                path_loss_interference = 69.55 + 26.16 * log10_f - 13.82 * log10_hb - correction + (
                        44.9 - 6.55 * log10_hb) * math.log10(distance)
                # logging.debug('path_loss_interference: %s ' % path_loss_interference)
                interference_dB = Tx_dBm - path_loss_interference
                # logging.debug('interference_dB: %s ' % interference_dB)
                interference_mW = math.pow(10, (interference_dB / 10))
                # logging.debug('interference_mW: %s ' % interference_mW)
                total_interference_mW = total_interference_mW + interference_mW
        # logging.debug('total_interference_mW: %s' % total_interference_mW)

        # calculate SINR
        if distance_to_actual < 0.0009:
            distance_to_actual = 0.001
        path_loss_hata = 69.55 + 26.16 * log10_f - 13.82 * log10_hb - correction + (
                44.9 - 6.55 * log10_hb) * math.log10(distance_to_actual)
        # logging.debug('path_loss_hata: %s ' % path_loss_hata)
        P_received_dB = Tx_dBm - path_loss_hata
        # logging.debug('P_received_dB: %s ' % P_received_dB)
        P_received_mW = math.pow(10, (P_received_dB / 10))
        # logging.debug('P_received_mW: %s ' % P_received_mW)
        # P_noise_mW = BOLTZMAN * BANDWIDTH * THERMODYN * 0.001
        # logging.debug('P_noise_mW: %s ' % P_noise_mW)
        sinr = P_received_mW / (total_interference_mW + self.NOISE_TARAS)  # P_noise_mW)
        # logging.debug('sinr: %s ' % sinr)
        # sinr_dB = 10 * math.log10(sinr)
        # logging.debug('sinr_dB: %s ' % sinr_dB)

        # throughput:
        throughput = 180000 * math.log2(1 + sinr) * 1  # 1 resource_block will got everyone
        end_user.smallcells.loc[actual_smallcell, 'throughput_1RB'] = throughput
        # logging.debug("throughput to smallcell: %s "% throughput)
        return throughput
