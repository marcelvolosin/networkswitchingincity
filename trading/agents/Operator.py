import pandas as pd

from src.common.UniqueID import UniqueID


class Operator():
    def __init__(self, net, owner='MACROCELLS'):
        uid = UniqueID()
        self.id = uid.getId()
        self.net = net

        self.throughput_to_connected = None
        self.retail_prices_probabilities = pd.DataFrame(columns=['retail_price', 'new_probability', 'old_probability'])
        self.new_retail_price = None
        self.macrocells = []
        self.smallcells = []
        self.cost = 0
        self.income = 0
        self.profit = 0
        self.owner = owner
        self.strategy = None

    def step(self):
        self.cost = 0
        self.income = 0
        self.profit = 0

    def getDetails(self):
        data = {}
        data["id"] = self.id
        data["net"] = self.net
        data["throughou_to_connected"] = self.throughput_to_connected
        data["new_retail_price"] = self.new_retail_price
        data["cost"] = self.cost
        data["income"] = self.income
        data["profit"] = self.profit
        data["owner"] = self.owner
        data["strategy"] = self.strategy

        macrocells = []
        for cell in self.macrocells + self.smallcells:
            macrocells.append(cell.getGeoJson())
        data["cells"] = macrocells
        return data
