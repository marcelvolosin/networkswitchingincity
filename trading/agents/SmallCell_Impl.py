from src.placeable.stationary.FemtoCell import FemtoCell
from trading.Settings import Settings


class Smallcell_Impl(FemtoCell):

    def __init__(self):
        super(Smallcell_Impl, self).__init__()
        self.settings = Settings()
        self.owner = None
        self.type = "Femtocell"
        self.operator = None

        self.transmit_power_Pi = 6.3
        self.height_hb = 3.5
        self.carrier_frequency_f = 2100  # MHz
        self.strategy = None

        self.no_of_available_RBs = self.settings.SMALLCELL_RESOURCE_BLOCKS
        self.potential_end_users = []
        self.will_provide_rbs = None
        self.new_retail_price = None
        self.connected_end_users = []
        self.retail_prices_probabilities = None
        self.income = 0
        self.cost = 0

    def getGeoJson(self):
        data = super(Smallcell_Impl, self).getGeoJson()
        data['strategy'] = self.strategy

        # users = []
        # for user in self.potential_end_users:
        #     users.append(user.getGeoJson())
        #
        # data['potential_end_users']=users
        data['will_provide_rbs'] = self.will_provide_rbs
        data['new_retail_price'] = self.new_retail_price

        # connected = []
        # for user in self.connected_end_users:
        #     connected.append(user.getGeoJson())
        # data['connected_end_users'] = self.connected_end_users
        data['income'] = self.income
        data['cost'] = self.cost
        return data

    def step(self):
        self.potential_end_users = []
        self.will_provide_rbs = None
        # self.new_retail_price = None
        self.connected_end_users = []
        self.income = 0
        self.cost = 0