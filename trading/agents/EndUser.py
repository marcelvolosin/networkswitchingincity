from src.placeable.movable.walkable.Person import Person
import pandas as pd


class EndUser(Person):

    def __init__(self):
        super(EndUser, self).__init__()
        self.height_hb = 1.7

        self.smallcells = pd.DataFrame(columns=['distance', 'throughput_1RB'])
        self.macrocells = pd.DataFrame(columns=['distance', 'throughput_1RB'])
        self.potential_macrocell_to_connect = None
        self.potential_smallcell_to_connect = None
        # self.maximum_price = model.CRN.MAXIMUM_PRICE
        # self.minimal_throughput = model.CRN.MINIMAL_THROUGHPUT
        self.throughput_to_connected = 0
        self.new_retail_price = None
        self.owner = None
        self.Rm = 0
        self.Rs = 0
        self.Um = 0
        self.Us = 0
        self.connected_to_cell = None
        self.retail_prices_probabilities = None
        self.sensing_macrocell_rbs = 0
        self.sensing_smallcell_rbs = 0
        self.potential_macrocell_to_connect = None
        self.potential_smallcell_to_connect = None
        self.assigned_rbs = 0

        self.unique_id = self.id

    def getGeoJson(self):
        data = super(EndUser, self).getGeoJson()
        data['throughput_to_connected'] = self.throughput_to_connected
        data['new_retail_price'] = self.new_retail_price
        data['owner'] = self.owner
        data['Rm'] = self.Rm
        data['Rs'] = self.Rs
        data['Um'] = self.Um
        data['Us'] = self.Us
        data['connected_to_cell'] = self.connected_to_cell
        data['assigned_rbs'] = self.assigned_rbs
        return data

    def step(self):
        #pos = random_walk(self, 2)
        # while not self.model.grid.is_cell_empty(pos):
        #     pos = random_walk(self, 2)
        #self.model.grid.move_agent(self, pos)
        self.connected_to_cell = None
        self.Rm = 0
        self.Rs = 0
        self.Um = 0
        self.Us = 0
        self.sensing_macrocell_rbs = 0
        self.sensing_smallcell_rbs = 0
        self.potential_macrocell_to_connect = None
        self.potential_smallcell_to_connect = None
        self.assigned_rbs = 0