from src.placeable.stationary.MacroCell import MacroCell
from trading.Settings import Settings


class Macrocell_Impl(MacroCell):

    def __init__(self, patternObject=MacroCell):
        super(Macrocell_Impl, self).__init__()

        self.type = "Macrocell"
        # copy details of original macrocell loaded from OpencellID
        self.location = patternObject.location
        self.openCellId = patternObject.openCellId
        self.created_at = patternObject.created_at
        self.updated_at = patternObject.updated_at
        self.radio = patternObject.radio
        self.mcc = patternObject.mcc
        self.net = patternObject.net
        self.area = patternObject.area
        self.cell = patternObject.cell
        self.unit = patternObject.unit
        self.range = patternObject.range
        self.samples = patternObject.samples
        self.changeable = patternObject.changeable
        self.created = patternObject.created
        self.updated = patternObject.updated
        self.averageSignal = patternObject.averageSignal
        self.onBuilding = patternObject.onBuilding

        self.settings = Settings()
        self.owner = None
        self.operator = None

        self.transmit_power_Pi = 40.5  # W
        self.height_hb = 25  # m
        self.carrier_frequency_f = 2100  # MHz
        self.strategy = None

        self.no_of_available_RBs = self.settings.MACROCELL_RESOURCE_BLOCKS
        self.potential_end_users = []
        self.can_provide_rbs = None
        self.will_provide_rbs = None
        self.new_retail_price = None
        self.connected_end_users = []
        self.retail_prices_probabilities = None
        self.income = 0
        self.cost = 0

    def getGeoJson(self):
        data = super(Macrocell_Impl, self).getGeoJson()
        data['strategy'] = self.strategy

        # users = []
        # for user in self.potential_end_users:
        #     users.append(user.getGeoJson())
        # data['potential_end_users']=users

        data['can_provide_rbs'] = self.can_provide_rbs
        data['will_provide_rbs'] = self.will_provide_rbs
        data['new_retail_price'] = self.new_retail_price

        # connected = []
        # for user in self.connected_end_users:
        #     connected.append(user.getGeoJson())
        # data['connected_end_users'] = self.connected_end_users
        data['income'] = self.income
        data['cost'] = self.cost
        return data

    def step(self):
        self.potential_end_users = []
        self.can_provide_rbs = None
        self.will_provide_rbs = None
        # self.new_retail_price = None
        self.connected_end_users = []
        self.income = 0
        self.cost = 0
